package com.bloomtech.wms;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootTest
@EnableMongoRepositories(basePackages = { "com.bloomtech.wms" })
class WMSApplicationTests
{

	@Test
	void contextLoads()
	{
	}

}
