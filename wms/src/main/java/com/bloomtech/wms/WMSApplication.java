package com.bloomtech.wms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { ErrorMvcAutoConfiguration.class })
@ComponentScan(basePackages = { "com.bloomtech" })
public class WMSApplication
{
	private static final Logger LOGGER = LoggerFactory.getLogger(WMSApplication.class);

	public static void main(String[] args)
	{
		SpringApplication.run(WMSApplication.class, args);
		LOGGER.info("WMSApplication Started");
	}

}
