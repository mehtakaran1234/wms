package com.bloomtech.wms.dao;

import java.util.List;

import com.bloomtech.wms.model.Account;

public interface AccountDao
{

	Account createAccount(Account account);

	List<Account> getAllAccount(String title);

}
