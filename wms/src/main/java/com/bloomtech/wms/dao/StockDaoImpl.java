package com.bloomtech.wms.dao;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.IteratorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bloomtech.wms.model.Stock;
import com.bloomtech.wms.repo.StockRepository;

@Repository
public class StockDaoImpl implements StockDao
{

	Logger logger = LoggerFactory.getLogger(StockDaoImpl.class);

	@Autowired
	StockRepository stockRepository;

	@Override
	public void saveStock() throws Exception
	{
		stockRepository.save(new Stock("Colgate", 5));
		stockRepository.save(new Stock("Coffee", 2));
		stockRepository.save(new Stock("SSD drive", 200));
		stockRepository.save(new Stock("Headphones", 350));
		stockRepository.save(new Stock("Mouse", 5));
	}

	@Override
	public List<Stock> getStockListByItem(String itemToFind) throws Exception
	{
		Iterable<Stock> iterator = stockRepository.findAll();

		logger.debug("All stock items: ");
		iterator.forEach(item -> logger.debug(item.toString()));

		List<Stock> stockList = stockRepository.findByItem(itemToFind);
		logger.debug("\nHow does my stock cost?: ");
		stockList.forEach(item -> logger.debug(item.toString()));
		return stockList;
	}

	@Override
	public List<Stock> getAllStockByPriceOver(int price) throws Exception
	{
		Iterable<Stock> iterator = stockRepository.findAll();

		logger.debug("All stock items: ");
		iterator.forEach(item -> logger.debug(item.toString()));

		List<Stock> stockItems = stockRepository.listItemsWithPriceOver(price);
		logger.debug("\nExpensive Items: ");
		stockItems.forEach(item -> logger.debug(item.toString()));
		return stockItems;

	}

	@Override
	public List<Stock> getAllStock() throws Exception
	{
		Iterable<Stock> iterable = stockRepository.findAll();
		Iterator<Stock> iterator = iterable.iterator();
		logger.debug("All stock items: ");
		iterable.forEach(item -> logger.debug(item.toString()));

		List<Stock> stockItems = IteratorUtils.toList(iterator);
		return stockItems;

	}

}
