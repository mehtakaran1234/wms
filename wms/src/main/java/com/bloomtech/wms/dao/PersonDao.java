package com.bloomtech.wms.dao;

import java.util.List;

import com.bloomtech.wms.model.Person;

public interface PersonDao
{

	List<Person> getPersonListByItem(String lastName) throws Exception;

	void savePerson(Person person);

	List<Person> getPersonList();

	void updatePerson(Person person);
}
