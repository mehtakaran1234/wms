package com.bloomtech.wms.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bloomtech.wms.model.Person;
import com.bloomtech.wms.repo.PersonRepository;

@Repository
public class PersonDaoImpl implements PersonDao
{
	Logger logger = LoggerFactory.getLogger(PersonDaoImpl.class);

	@Autowired
	PersonRepository personRepository;

	@Override
	public List<Person> getPersonListByItem(String lastName) throws Exception
	{
		logger.debug("All Person By Name: ");
		List<Person> personList = personRepository.findByLastName(lastName);
		return personList;
	}

	@Override
	public void savePerson(Person person)
	{
		personRepository.save(person);

	}

	@Override
	public void updatePerson(Person person)
	{
		// personRepository.save(person);

		/*
		 * user = mongoTemplate.findOne( Query.query(Criteria.where("name").is("Jack")),
		 * User.class); user.setName("Jim"); mongoTemplate.save(user, "user");
		 */

	}

	@Override
	public List<Person> getPersonList()
	{
		List<Person> personList = personRepository.findAll();
		return personList;
	}
}
