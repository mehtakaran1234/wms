package com.bloomtech.wms.dao;

import java.util.List;

import com.bloomtech.wms.model.Article;

public interface ArticleDao
{

	Article createArticle(Article article);

	List<Article> getAllArticle(String title);

}
