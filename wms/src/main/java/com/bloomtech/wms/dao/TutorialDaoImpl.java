package com.bloomtech.wms.dao;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bloomtech.wms.model.Tutorial;
import com.bloomtech.wms.repo.TutorialRepository;

@Repository
public class TutorialDaoImpl implements TutorialDao
{
	Logger logger = LoggerFactory.getLogger(TutorialDaoImpl.class);

	@Autowired
	TutorialRepository tutorialRepository;

	@Override
	public List<Tutorial> getAllTutorials(String title)
	{

		if (title == null)
			return tutorialRepository.findAll();
		else
			return tutorialRepository.findByTitleContaining(title);

	}

	@Override
	public Tutorial getTutorialById(String id)
	{
		Optional<Tutorial> tutorialOptional = tutorialRepository.findById(id);
		if (tutorialOptional.isPresent())
		{
			return tutorialOptional.get();
		}
		else
		{
			return null;
		}
	}

	@Override
	public Tutorial createTutorial(Tutorial tutorial)
	{
		return tutorialRepository.save(tutorial);
	}

	@Override
	public Tutorial updateTutorial(String id, Tutorial tutorial)
	{
		Optional<Tutorial> tutorialData = tutorialRepository.findById(id);

		if (tutorialData.isPresent())
		{
			Tutorial _tutorial = tutorialData.get();
			_tutorial.setTitle(tutorial.getTitle());
			_tutorial.setDescription(tutorial.getDescription());
			_tutorial.setPublished(tutorial.isPublished());
			return tutorialRepository.save(_tutorial);
		}
		else
		{
			return null;
		}
	}

	@Override
	public boolean deleteTutorial(String id)
	{
		try
		{
			tutorialRepository.deleteById(id);
			return true;
		}
		catch (Exception e)
		{
			logger.error("Internal Server Error has occured", e);
			return false;
		}
	}

	@Override
	public boolean deleteAllTutorials()
	{
		try
		{
			tutorialRepository.deleteAll();
			return true;
		}
		catch (Exception e)
		{
			logger.error("Internal Server Error has occured", e);
			return false;
		}
	}

	@Override
	public List<Tutorial> findByPublished()
	{
		return tutorialRepository.findByPublished(true);
	}

}
