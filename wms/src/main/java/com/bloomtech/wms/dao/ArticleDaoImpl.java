package com.bloomtech.wms.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bloomtech.wms.model.Article;
import com.bloomtech.wms.repo.ArticleRepository;

@Repository
public class ArticleDaoImpl implements ArticleDao
{
	@Autowired
	ArticleRepository articleRepository;

	@Override
	public List<Article> getAllArticle(String title)
	{

		if (title == null)
			return articleRepository.findAll();
		else
			return articleRepository.findByTitleContaining(title);

	}

	@Override
	public Article createArticle(Article article)
	{
		return articleRepository.save(article);
	}

}
