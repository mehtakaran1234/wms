package com.bloomtech.wms.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bloomtech.wms.model.Account;
import com.bloomtech.wms.repo.AccountRepository;

@Repository
public class AccountDaoImpl implements AccountDao
{
	@Autowired
	AccountRepository accountRepository;

	@Override
	public List<Account> getAllAccount(String title)
	{
		return accountRepository.findAll();
	}

	@Override
	public Account createAccount(Account account)
	{
		return accountRepository.save(account);
	}

}
