package com.bloomtech.wms.dao;

import java.util.List;

import com.bloomtech.wms.model.Stock;

public interface StockDao
{

	public void saveStock() throws Exception;

	public List<Stock> getStockListByItem(String itemToFind) throws Exception;

	public List<Stock> getAllStockByPriceOver(int price) throws Exception;

	public List<Stock> getAllStock() throws Exception;
}
