package com.bloomtech.wms.service;

import java.util.List;

import com.bloomtech.wms.model.Article;

public interface ArticleService
{

	Article createArticle(Article article);

	List<Article> getAllArticle(String title);
}
