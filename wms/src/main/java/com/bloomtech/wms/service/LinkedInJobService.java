package com.bloomtech.wms.service;

import java.util.List;

import com.bloomtech.wms.model.linkedin.Source;

public interface LinkedInJobService
{

	Source createJobs(String xmlJob);

	List<Source> getAllJobs(String jobId);

}
