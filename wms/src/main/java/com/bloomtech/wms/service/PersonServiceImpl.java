package com.bloomtech.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bloomtech.wms.dao.PersonDao;
import com.bloomtech.wms.model.Person;

@Service
public class PersonServiceImpl implements PersonService
{
	@Autowired
	PersonDao personDao;

	@Override
	public List<Person> getPersonListByLastName(String lastname) throws Exception
	{
		return personDao.getPersonListByItem(lastname);
	}

	@Override
	public void savePerson(Person person)
	{
		personDao.savePerson(person);

	}

	@Override
	public List<Person> getPersonList()
	{
		return personDao.getPersonList();
	}
}
