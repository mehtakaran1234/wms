package com.bloomtech.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bloomtech.wms.dao.StockDao;
import com.bloomtech.wms.model.Stock;

@Service
public class StockServiceImpl implements StockService
{
	@Autowired
	StockDao stockDao;

	@Override
	public void saveStock() throws Exception
	{
		stockDao.saveStock();
	}

	@Override
	public List<Stock> getStockListByItem(String itemToFind) throws Exception
	{
		return stockDao.getStockListByItem(itemToFind);
	}

	@Override
	public List<Stock> getAllStockByPriceOver(int price) throws Exception
	{
		return stockDao.getAllStockByPriceOver(price);
	}

	@Override
	public List<Stock> getAllStock() throws Exception
	{
		return stockDao.getAllStock();
	}
}
