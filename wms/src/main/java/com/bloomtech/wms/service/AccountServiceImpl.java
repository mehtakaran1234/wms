package com.bloomtech.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bloomtech.wms.dao.AccountDao;
import com.bloomtech.wms.model.Account;

@Service
public class AccountServiceImpl implements AccountService
{
	@Autowired
	AccountDao accountDao;

	@Override
	public List<Account> getAllAccount(String title)
	{
		return accountDao.getAllAccount(title);
	}

	@Override
	public Account createAccount(Account account)
	{
		return accountDao.createAccount(account);
	}

}
