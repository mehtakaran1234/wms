package com.bloomtech.wms.service;

import java.util.List;

import com.bloomtech.wms.model.Person;

public interface PersonService
{
	List<Person> getPersonListByLastName(String lastname) throws Exception;

	void savePerson(Person person);

	List<Person> getPersonList();
}
