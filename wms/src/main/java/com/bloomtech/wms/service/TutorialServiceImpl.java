package com.bloomtech.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bloomtech.wms.dao.TutorialDao;
import com.bloomtech.wms.model.Tutorial;

@Service
public class TutorialServiceImpl implements TutorialService
{
	@Autowired
	TutorialDao tutorialDao;

	@Override
	public List<Tutorial> getAllTutorials(String title)
	{
		return tutorialDao.getAllTutorials(title);
	}

	@Override
	public Tutorial getTutorialById(String id)
	{
		return tutorialDao.getTutorialById(id);
	}

	@Override
	public Tutorial createTutorial(Tutorial tutorial)
	{
		return tutorialDao.createTutorial(tutorial);
	}

	@Override
	public Tutorial updateTutorial(String id, Tutorial tutorial)
	{
		return tutorialDao.updateTutorial(id, tutorial);
	}

	@Override
	public boolean deleteTutorial(String id)
	{

		return tutorialDao.deleteTutorial(id);
	}

	@Override
	public boolean deleteAllTutorials()
	{
		return tutorialDao.deleteAllTutorials();
	}

	@Override
	public List<Tutorial> findByPublished()
	{
		return tutorialDao.findByPublished();
	}

}
