package com.bloomtech.wms.service;

import java.util.List;

import com.bloomtech.wms.model.Tutorial;

public interface TutorialService
{
	List<Tutorial> getAllTutorials(String title);

	Tutorial getTutorialById(String id);

	Tutorial createTutorial(Tutorial tutorial);

	Tutorial updateTutorial(String id, Tutorial tutorial);

	boolean deleteTutorial(String id);

	boolean deleteAllTutorials();

	List<Tutorial> findByPublished();
}
