package com.bloomtech.wms.service;

import java.util.List;

import com.bloomtech.wms.model.Stock;

public interface StockService
{

	public void saveStock() throws Exception;

	public List<Stock> getStockListByItem(String itemToFind) throws Exception;

	public List<Stock> getAllStockByPriceOver(int price) throws Exception;

	public List<Stock> getAllStock() throws Exception;
}
