package com.bloomtech.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bloomtech.wms.dao.ArticleDao;
import com.bloomtech.wms.model.Article;

@Service
public class ArticleServiceImpl implements ArticleService
{
	@Autowired
	ArticleDao articleDao;

	@Override
	public List<Article> getAllArticle(String title)
	{
		return articleDao.getAllArticle(title);
	}

	@Override
	public Article createArticle(Article article)
	{
		return articleDao.createArticle(article);
	}

}
