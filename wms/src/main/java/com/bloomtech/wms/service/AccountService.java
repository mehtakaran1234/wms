package com.bloomtech.wms.service;

import java.util.List;

import com.bloomtech.wms.model.Account;

public interface AccountService
{

	Account createAccount(Account account);

	List<Account> getAllAccount(String title);
}
