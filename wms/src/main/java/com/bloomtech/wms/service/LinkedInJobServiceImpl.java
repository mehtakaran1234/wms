package com.bloomtech.wms.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.bloomtech.wms.model.linkedin.Source;
import com.bloomtech.wms.util.XMLUtil;

@Service
public class LinkedInJobServiceImpl implements LinkedInJobService
{

	Logger logger = LoggerFactory.getLogger(LinkedInJobServiceImpl.class);

	@Override
	public Source createJobs(String xmlJob)
	{
		logger.debug("xmlJob: {}", xmlJob);
		XMLUtil.unmarshall(xmlJob, Source.class);
		return null;
	}

	@Override
	public List<Source> getAllJobs(String jobId)
	{

		return null;
	}

}
