package com.bloomtech.wms.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/home")
public class HomeController
{

	@GetMapping("/hc")
	public String healcth()
	{
		return "Running OK on my server";
	}
}
