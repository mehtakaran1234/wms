package com.bloomtech.wms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bloomtech.wms.model.Account;
import com.bloomtech.wms.service.AccountService;

@RestController
@RequestMapping("/api")
public class AccountController
{
	@Autowired
	AccountService accountService;

	// https://mflash.dev/blog/2019/07/08/persisting-documents-with-mongorepository/

	@GetMapping("/account")
	public @ResponseBody List<Account> getAllAccount(@RequestParam(required = false) String created)
	{
		return accountService.getAllAccount(created);
	}

	@PostMapping("/account")
	public @ResponseBody Account createArticle(@RequestBody Account account)
	{
		return accountService.createAccount(account);
	}
}
