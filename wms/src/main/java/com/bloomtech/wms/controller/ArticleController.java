package com.bloomtech.wms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bloomtech.wms.model.Article;
import com.bloomtech.wms.service.ArticleService;

@RestController
@RequestMapping("/api")
public class ArticleController
{
	@Autowired
	ArticleService articleService;

	@GetMapping("/article")
	public @ResponseBody List<Article> getAllArticle(@RequestParam(required = false) String title)
	{
		return articleService.getAllArticle(title);
	}

	@PostMapping("/article")
	public @ResponseBody Article createArticle(@RequestBody Article article)
	{
		return articleService.createArticle(article);
	}
}
