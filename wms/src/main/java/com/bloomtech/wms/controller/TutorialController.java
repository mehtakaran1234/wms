package com.bloomtech.wms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bloomtech.wms.model.Tutorial;
import com.bloomtech.wms.service.TutorialService;

@RestController
@RequestMapping("/api")
public class TutorialController
{

	@Autowired
	TutorialService tutorialService;

	@GetMapping("/tutorials")
	public @ResponseBody List<Tutorial> getAllTutorials(@RequestParam(required = false) String title)
	{
		return tutorialService.getAllTutorials(title);
	}

	@GetMapping("/tutorials/{id}")
	public @ResponseBody Tutorial getTutorialById(@PathVariable("id") String id)
	{
		return tutorialService.getTutorialById(id);
	}

	@PostMapping("/tutorials")
	public @ResponseBody Tutorial createTutorial(@RequestBody Tutorial tutorial)
	{
		return tutorialService.createTutorial(tutorial);
	}

	@PutMapping("/tutorials/{id}")
	public @ResponseBody Tutorial updateTutorial(@PathVariable("id") String id, @RequestBody Tutorial tutorial)
	{
		return tutorialService.updateTutorial(id, tutorial);
	}

	@DeleteMapping("/tutorials/{id}")
	public @ResponseBody boolean deleteTutorial(@PathVariable("id") String id)
	{
		return tutorialService.deleteTutorial(id);
	}

	@DeleteMapping("/tutorials")
	public @ResponseBody boolean deleteAllTutorials()
	{
		return tutorialService.deleteAllTutorials();
	}

	@GetMapping("/tutorials/published")
	public @ResponseBody List<Tutorial> findByPublished()
	{
		return tutorialService.findByPublished();
	}
}
