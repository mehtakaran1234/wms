package com.bloomtech.wms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bloomtech.wms.model.linkedin.Source;
import com.bloomtech.wms.service.LinkedInJobService;

@RestController
@RequestMapping("/api")
public class LinkedInJobController
{
	@Autowired
	LinkedInJobService linkedInJobService;

	@GetMapping("/jobs")
	public @ResponseBody List<Source> getAllJobs(@RequestParam(required = false) String jobId)
	{
		return linkedInJobService.getAllJobs(jobId);
	}

	@PostMapping(path = "/jobs", consumes = { MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody Source createJobs(@RequestBody String xmlJob)
	{
		return linkedInJobService.createJobs(xmlJob);
	}
}
