package com.bloomtech.wms.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bloomtech.wms.service.StockService;

@RestController
@RequestMapping("/stock/api")
public class StockController
{
	Logger logger = LoggerFactory.getLogger(StockController.class);

	@Autowired
	StockService stockService;

	@GetMapping("/createstock")
	public String createStock()
	{
		try
		{
			stockService.saveStock();
			return "Stock Saved Successfully";
		}
		catch (Exception e)
		{

			return "Some Error Occured with HTTP status: " + HttpStatus.INTERNAL_SERVER_ERROR.toString();
		}

	}

	@GetMapping("/liststock")
	public String getStockList()
	{
		try
		{
			logger.info("Calling controller");
			return stockService.getAllStock().toString();
		}
		catch (Exception e)
		{
			return "Some Error Occured with HTTP status: " + HttpStatus.INTERNAL_SERVER_ERROR.toString();
		}
	}

	@GetMapping("/liststock/{itemToFind}")
	public String getStockList(@PathVariable String itemToFind)
	{
		try
		{
			return stockService.getStockListByItem(itemToFind).toString();
		}
		catch (Exception e)
		{
			return "Some Error Occured with HTTP status: " + HttpStatus.INTERNAL_SERVER_ERROR.toString();
		}
	}
}
