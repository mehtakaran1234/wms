package com.bloomtech.wms.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bloomtech.wms.model.Person;
import com.bloomtech.wms.service.PersonService;

@RestController
@RequestMapping("/person/api")
public class PersonController
{
	Logger logger = LoggerFactory.getLogger(PersonController.class);

	@Autowired
	PersonService personService;

	@GetMapping("/listperson")
	public String getPersonList()
	{
		try
		{
			return personService.getPersonList().toString();
		}
		catch (Exception e)
		{
			return "Some Error Occured with HTTP status: " + HttpStatus.INTERNAL_SERVER_ERROR.toString();
		}
	}

	@GetMapping("/listperson/{lastName}")
	public String getPersonListByLastName(@PathVariable String lastName)
	{
		try
		{
			return personService.getPersonListByLastName(lastName).toString();
		}
		catch (Exception e)
		{
			return "Some Error Occured with HTTP status: " + HttpStatus.INTERNAL_SERVER_ERROR.toString();
		}
	}

	@PostMapping("/createPerson")
	public String createPerson(@RequestBody Person person)
	{
		try
		{
			logger.info("person: " + person);
			personService.savePerson(person);
			return "Person Saved Successfully";
		}
		catch (Exception e)
		{

			return "Some Error Occured with HTTP status: " + HttpStatus.INTERNAL_SERVER_ERROR.toString();
		}

	}
}
