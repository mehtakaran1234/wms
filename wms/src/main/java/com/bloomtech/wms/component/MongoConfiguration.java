package com.bloomtech.wms.component;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.bloomtech.wms.event.AccountCascadeMongoEventListener;

@EnableMongoRepositories(MongoConfiguration.REPOSITORY_PACKAGE)
public @Configuration class MongoConfiguration
{

	static final String REPOSITORY_PACKAGE = "com.bloomtech.wms.repo";

	public @Bean AccountCascadeMongoEventListener cascadeMongoEventListener()
	{
		return new AccountCascadeMongoEventListener();
	}

	public @Bean MongoCustomConversions customConversions()
	{
		return new MongoCustomConversions(ZonedDateTimeConverters.getConvertersToRegister());
	}
}
