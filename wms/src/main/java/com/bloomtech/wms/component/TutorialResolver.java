package com.bloomtech.wms.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bloomtech.wms.model.Author;
import com.bloomtech.wms.model.Tutorial;
import com.bloomtech.wms.repo.AuthorRepository;
import com.coxautodev.graphql.tools.GraphQLResolver;

@Component
public class TutorialResolver implements GraphQLResolver<Tutorial>
{
	@Autowired
	private AuthorRepository authorRepository;

	public TutorialResolver(AuthorRepository authorRepository)
	{
		this.authorRepository = authorRepository;
	}

	public Author getAuthor(Tutorial tutorial)
	{
		return authorRepository.findById(tutorial.getAuthorId()).orElseThrow(null);
	}
}
