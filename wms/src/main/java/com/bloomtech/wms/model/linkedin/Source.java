package com.bloomtech.wms.model.linkedin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

// import com.fasterxml.jackson.databind.ObjectMapper; // version 2.11.1
// import com.fasterxml.jackson.annotation.JsonProperty; // version 2.11.1

@XmlRootElement(name = "Source")
@XmlAccessorType(XmlAccessType.FIELD)
public class Source
{
	public String lastBuildDate;
	public String publisherUrl;
	public String publisher;
	public ExpectedJobCount expectedJobCount;
	public Job job;
}
