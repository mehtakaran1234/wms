package com.bloomtech.wms.model.linkedin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HowToApply
{
	@JsonProperty("application-url")
	public String applicationUrl;
}
