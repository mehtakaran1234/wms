package com.bloomtech.wms.model;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.UUID;

import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;

@Data
@Builder
public class Session
{

	private final @Id @Default String key = UUID.randomUUID().toString();
	private String city;
	private Locale locale;
	private LocalDateTime accessed;

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public Locale getLocale()
	{
		return locale;
	}

	public void setLocale(Locale locale)
	{
		this.locale = locale;
	}

	public LocalDateTime getAccessed()
	{
		return accessed;
	}

	public void setAccessed(LocalDateTime accessed)
	{
		this.accessed = accessed;
	}

	public String getKey()
	{
		return key;
	}

	@Override
	public String toString()
	{
		return "Session [key=" + key + ", city=" + city + ", locale=" + locale + ", accessed=" + accessed + "]";
	}

}
