package com.bloomtech.wms.model.linkedin;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JobFunctions
{
	@JsonProperty("job-function")
	public List<JobFunction> jobFunction;
}
