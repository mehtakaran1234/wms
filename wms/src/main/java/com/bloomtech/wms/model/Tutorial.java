package com.bloomtech.wms.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tutorials")
public class Tutorial
{
	@Id
	private String id;
	private String title;
	private String description;
	private String author_id;
	private boolean published;

	public Tutorial()
	{
	}

	public Tutorial(String title, String description, String author_id, boolean published)
	{
		this.title = title;
		this.description = description;
		this.author_id = author_id;
		this.published = published;
	}

	public String getId()
	{
		return id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getAuthorId()
	{
		return author_id;
	}

	public void setAuthorId(String author_id)
	{
		this.author_id = author_id;
	}

	public boolean isPublished()
	{
		return published;
	}

	public void setPublished(boolean isPublished)
	{
		this.published = isPublished;
	}

	@Override
	public String toString()
	{
		return "Tutorial [id=" + id + ", title=" + title + ", description=" + description + ", published=" + published
				+ ", author_id=" + author_id + "]";
	}
}