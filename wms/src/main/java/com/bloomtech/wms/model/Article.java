package com.bloomtech.wms.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.bloomtech.wms.interfaces.CascadeSave;

@Document(collection = "article")
public class Article
{
	@Id
	private String id;
	@DBRef(lazy = true)
	@CascadeSave
	private List<Comment> comments;
	private String title;
	private String text;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public List<Comment> getComments()
	{
		return comments;
	}

	public void setComments(List<Comment> comments)
	{
		this.comments = comments;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	@Override
	public String toString()
	{
		return "Article [id=" + id + ", comments=" + comments + ", title=" + title + ", text=" + text + "]";
	}

}
