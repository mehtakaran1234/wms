package com.bloomtech.wms.model;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import com.bloomtech.wms.event.Cascade;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.Singular;

@Data
@Builder
public class Account
{
	private final @Id @Default String key = UUID.randomUUID().toString();

	@DBRef
	@Cascade
	private User user;

	@DBRef
	@Cascade
	@Singular
	private Set<Session> sessions;

	private ZonedDateTime created;

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public Set<Session> getSessions()
	{
		return sessions;
	}

	public void setSessions(Set<Session> sessions)
	{
		this.sessions = sessions;
	}

	public ZonedDateTime getCreated()
	{
		return created;
	}

	public void setCreated(ZonedDateTime created)
	{
		this.created = created;
	}

	public String getKey()
	{
		return key;
	}

	@Override
	public String toString()
	{
		return "Account [key=" + key + ", user=" + user + ", sessions=" + sessions + ", created=" + created + "]";
	}

}
