package com.bloomtech.wms.model.linkedin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Location
{
	public Country country;
	@JsonProperty("postal-code")
	public int postalCode;
	public String name;
}
