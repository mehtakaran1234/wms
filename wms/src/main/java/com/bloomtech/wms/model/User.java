package com.bloomtech.wms.model;

import java.time.LocalDate;
import java.util.Locale;
import java.util.UUID;

import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;

@Data
@Builder
public class User
{

	@Id
	@Default
	private final String key = UUID.randomUUID().toString();
	private String name;
	private String email;
	private Locale locale;
	private LocalDate dateOfBirth;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public Locale getLocale()
	{
		return locale;
	}

	public void setLocale(Locale locale)
	{
		this.locale = locale;
	}

	public LocalDate getDateOfBirth()
	{
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
	}

	public String getKey()
	{
		return key;
	}

	@Override
	public String toString()
	{
		return "User [key=" + key + ", name=" + name + ", email=" + email + ", locale=" + locale + ", dateOfBirth="
				+ dateOfBirth + "]";
	}

}
