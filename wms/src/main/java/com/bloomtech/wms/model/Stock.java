package com.bloomtech.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stock", schema = "mysqldb")
public class Stock
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String item;
	private float amount;

	protected Stock()
	{
	}

	public Stock(String item, float amount)
	{
		this.item = item;
		this.amount = amount;
	}

	// getters and setters are hidden for brevity

	public String getItem()
	{
		return item;
	}

	public void setItem(String item)
	{
		this.item = item;
	}

	public float getAmount()
	{
		return amount;
	}

	public void setAmount(float amount)
	{
		this.amount = amount;
	}

	@Override
	public String toString()
	{
		return id + ". " + item + " - " + amount + " USD";
	}
}
