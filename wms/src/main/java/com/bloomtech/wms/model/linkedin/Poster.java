package com.bloomtech.wms.model.linkedin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Poster
{
	public boolean display;
	public Role role;
	@JsonProperty("email-address")
	public String emailAddress;
}
