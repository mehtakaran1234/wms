package com.bloomtech.wms.model.linkedin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Job
{
	@JsonProperty("partner-job-id")
	public String partnerJobId;
	@JsonProperty("contract-id")
	public int contractId;
	@JsonProperty("customer-job-code")
	public String customerJobCode;
	public Company company;
	public Position position;
	public String salary;
	@JsonProperty("referral-bonus")
	public String referralBonus;
	public Poster poster;
	@JsonProperty("how-to-apply")
	public HowToApply howToApply;
	@JsonProperty("tracking-pixel-url")
	public String trackingPixelUrl;
}
