package com.bloomtech.wms.model.linkedin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Position
{
	public String title;
	public String description;
	@JsonProperty("skills-and-experience")
	public String skillsAndExperience;
	public Location location;
	@JsonProperty("job-functions")
	public JobFunctions jobFunctions;
	public Industries industries;
	@JsonProperty("job-type")
	public JobType jobType;
	@JsonProperty("experience-level")
	public ExperienceLevel experienceLevel;
}
