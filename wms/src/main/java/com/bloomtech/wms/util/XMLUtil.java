package com.bloomtech.wms.util;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.bloomtech.wms.model.linkedin.Source;

public class XMLUtil
{

	public static <T> Object unmarshall(String xml, Class<T> clazz)
	{
		try
		{
			JAXBContext jc = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			StringReader sr = new StringReader(xml);
			Object obj = unmarshaller.unmarshal(sr);
			System.out.println(obj);
			return obj;
		}
		catch (JAXBException e)
		{

			System.err.println(e);
		}
		return null;
	}

	public static void main(String args[])
	{
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><source>    <lastBuildDate>Thu, 11 Sep 2014 19:39:59 GMT</lastBuildDate>    <publisherUrl>[https://www.linkedin.com%3c/publisherUrl%3e]https://www.linkedin.com</publisherUrl>    <publisher>LinkedIn</publisher>   <expectedJobCount><![CDATA[5596]]></expectedJobCount>     <job>  <partner-job-id>LIJ-13239292</partner-job-id>  <contract-id>1234</contract-id>  <customer-job-code>DE247X</customer-job-code>  <company>    <id>1234</id>    <name>Company ABC</name>    <description>A great company</description>  </company>  <position>    <title>Chief Architect</title>    <description>This is a great job.</description>    <skills-and-experience>Programming, financial analysis, and  thought leadership.</skills-and-experience>    <location>     <country>       <code>us</code>      </country>      <postal-code>10012</postal-code>      <name>Midtown Manhattan</name>    </location>    <job-functions>      <job-function>        <code>acct</code>      </job-function>            <job-function>              <code>dsgn</code>            </job-function>        </job-functions>    <industries>      <industry>        <code>38</code>      </industry>      <industry>        <code>44</code>      </industry>    </industries>    <job-type>      <code>C</code>    </job-type>    <experience-level>      <code>4</code>    </experience-level>  </position>  <salary>$100,000-120,000 per year</salary>  <referral-bonus>$5,000 for employees</referral-bonus>  <poster>    <display>true</display>    <role>      <code>R</code>    </role>        <email-address>user@contract.com</email-address>  </poster>  <how-to-apply>    <application-url>http://www.linkedin.com</application-url>  </how-to-apply>  <tracking-pixel-url>http://www.linkedin.com/track.gif</tracking-pixel-url></job></source>";
		unmarshall(xml, Source.class);
	}
}
