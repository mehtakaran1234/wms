package com.bloomtech.wms.util;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.bloomtech.wms.model.Account;
import com.bloomtech.wms.model.Article;
import com.bloomtech.wms.model.Comment;
import com.bloomtech.wms.model.Person;
import com.bloomtech.wms.model.Session;
import com.bloomtech.wms.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONUtil
{
	public static void main(String[] a)
	{

		Person person = getPersonData();

		System.out.println(objectToJSON(person));

		Article article = getArticleData();

		System.out.println(objectToJSON(article));

		Account account = getAccountData();

		System.out.println(objectToJSON(account));
	}

	private static Account getAccountData()
	{
		User SAMPLE_USER = User.builder().name("Jasmine Beck").email("jasmine@example.com").locale(Locale.FRANCE)
				.dateOfBirth(LocalDate.of(1995, Month.DECEMBER, 12)).build();
		Session SAMPLE_SESSION = Session.builder().city("Paris").locale(Locale.FRANCE).build();
		Account SAMPLE_ACCOUNT = Account.builder().user(SAMPLE_USER).session(SAMPLE_SESSION)
				.created(ZonedDateTime.now()).build();
		return SAMPLE_ACCOUNT;
	}

	private static Article getArticleData()
	{
		Article article = new Article();
		article.setText("Arijit");
		article.setTitle("Hawayein");
		Comment comment = new Comment();
		comment.setAuthor("KM");
		comment.setText("KM Text");
		List<Comment> commentList = new ArrayList<Comment>();
		commentList.add(comment);
		article.setComments(commentList);

		return article;
	}

	// Get the data to be inserted into the object
	public static Person getPersonData()
	{

		Person person = new Person();
		person.setFirstName("GeeksforGeeks");
		person.setLastName("A computer Science portal for Geeks");

		// Return the object
		return person;
	}

	public static String objectToJSON(Object o)
	{

		ObjectMapper mapperObj = new ObjectMapper();
		mapperObj.findAndRegisterModules();
		String jsonStr = "";
		try
		{
			jsonStr = mapperObj.writeValueAsString(o);

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return jsonStr;
	}
}
