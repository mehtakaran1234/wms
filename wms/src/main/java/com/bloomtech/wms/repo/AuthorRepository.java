package com.bloomtech.wms.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.bloomtech.wms.model.Author;

public interface AuthorRepository extends MongoRepository<Author, String>
{

}
