package com.bloomtech.wms.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.bloomtech.wms.model.User;

public interface UserRepository extends MongoRepository<User, String>
{

}
