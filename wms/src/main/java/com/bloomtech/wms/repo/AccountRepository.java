package com.bloomtech.wms.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.bloomtech.wms.model.Account;
import com.bloomtech.wms.model.Session;
import com.bloomtech.wms.model.User;

public interface AccountRepository extends MongoRepository<Account, String>
{

	Account findDistinctFirstByUser(User user);

	List<Account> findBySessions(Session session);
}
