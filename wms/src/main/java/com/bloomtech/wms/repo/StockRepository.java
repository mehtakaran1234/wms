package com.bloomtech.wms.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.bloomtech.wms.model.Stock;

public interface StockRepository extends CrudRepository<Stock, Long>
{
	public List<Stock> findByItem(String item);

	@Query("SELECT e FROM Stock e WHERE e.amount >= :amount")
	public List<Stock> listItemsWithPriceOver(@Param("amount") float amount);
}
