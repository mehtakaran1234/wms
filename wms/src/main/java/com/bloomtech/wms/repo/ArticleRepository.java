package com.bloomtech.wms.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.bloomtech.wms.model.Article;

public interface ArticleRepository extends MongoRepository<Article, String>
{
	List<Article> findByTitleContaining(String title);
}
