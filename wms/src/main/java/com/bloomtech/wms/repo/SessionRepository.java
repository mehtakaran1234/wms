package com.bloomtech.wms.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.bloomtech.wms.model.Session;

public interface SessionRepository extends MongoRepository<Session, String>
{

}
