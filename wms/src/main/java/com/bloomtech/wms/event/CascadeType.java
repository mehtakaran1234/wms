package com.bloomtech.wms.event;

public enum CascadeType
{
	ALL, SAVE, DELETE
}
