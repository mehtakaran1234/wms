package com.bloomtech.wms.event;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Objects;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldCallback;

import com.bloomtech.wms.model.Account;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CascadeDeleteCallback implements FieldCallback
{

	private final Object source;
	private final MongoOperations mongoOperations;
	private final Account deletedAccount;

	public CascadeDeleteCallback(Object source, Account deletedAccount, MongoOperations mongoOperations)
	{
		this.source = source;
		this.mongoOperations = mongoOperations;
		this.deletedAccount = deletedAccount;
	}

	public @Override void doWith(final Field field) throws IllegalArgumentException, IllegalAccessException
	{
		ReflectionUtils.makeAccessible(field);

		if (field.isAnnotationPresent(DBRef.class) && field.isAnnotationPresent(Cascade.class))
		{
			final Object fieldValue = field.get(source);

			if (Objects.nonNull(fieldValue))
			{
				final var callback = new IdentifierCallback();
				final CascadeType cascadeType = field.getAnnotation(Cascade.class).value();

				if (cascadeType.equals(CascadeType.DELETE) || cascadeType.equals(CascadeType.ALL))
				{
					if (fieldValue instanceof Collection<?>)
					{
						((Collection<?>) fieldValue).forEach(mongoOperations::remove);
					}
					else
					{
						ReflectionUtils.doWithFields(fieldValue.getClass(), callback);
						mongoOperations.remove(fieldValue);
					}
				}
			}
		}
	}
}
