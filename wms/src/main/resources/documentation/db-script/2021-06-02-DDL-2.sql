-- JIRA Story	- WMS-6
-- Description	- This script is use to create the stock master table
-- Reviewd By	- Karan Mehta

CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(45) NOT NULL,
  `amount` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;